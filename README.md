# Automation of token transfers. Direct Debit on Solidity Smart Contracts.

An ERC20 token compliant smart contract system.

## [Smart Contracts](contracts)

To migrate:
- Edit [truffle-config.js](truffe-config.js) with the desired network.
- Run the following command from the base directory: 
```$ ./node_modules/.bin/truffle migrate --network <network name>```

This migration preseeds the first account configured in the node with a large number of tokens, and gives some to the rest of the accounts. Configurable in [this file](migrations/2_deploy_token.js).


## [Blockchain tests](src/new_measures)
Client side scripts to test an Ethereum node performance using the token trade system.
- [Main script](src/new_measures/index.js): Sends bursts of transactions (token transfers between different accounts) and collects metrics.
- [Account generation](src/new_measures/generate_accounts.js): Generates random ethereum accounts in a JSON file. These accounts will be used by the main script.
- [Account token seeding](src/new_measures/seed_accounts.js): Seeds the accounts in the JSON file with tokens. If the accounts are unseeded the main script will fail. 
